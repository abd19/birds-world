import 'package:flutter/material.dart';

class CustomDialog {
  static showAlertDialog(BuildContext context,
      String titleText, Widget contentWidget,Widget okButton) {
    // set up the button

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(titleText),
      content: contentWidget,
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
