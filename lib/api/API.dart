import 'dart:convert';
import 'package:project2/model/UserModel.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class API {
  static SharedPreferences? _sharedPreferences;
  Map<String, Map<String, String>>? messages;

  static String _url = "http://192.168.3.235:5000/API/";

  static Future<int> register() async {
    _sharedPreferences = await SharedPreferences.getInstance();
    var encodeBody = jsonEncode({
      "username": User.userName,
      "childName": User.childName,
      "password": User.password,
      "email": User.email
    });
    var response =
        await http.post(Uri.parse(_url + "register"), body: encodeBody);
    print(response.body);
    var decodeResponse = jsonDecode(response.body);
    if (decodeResponse["status"]["type"] == "success") {
      _sharedPreferences!.setString("username", User.userName);
      _sharedPreferences!.setString("childName", User.childName);
      _sharedPreferences!.setString("email", User.email);
      _sharedPreferences!.setBool("isLogged", true);

      return 1;
    } else if (decodeResponse["status"]["message"] == "username already taken")
      return -2;
    else if (decodeResponse["status"]["message"] == "email already taken")
      return -1;
    else
      return 0;
  }

  static Future<int> logIn(String username, String password) async {
    _sharedPreferences = await SharedPreferences.getInstance();
    var encodeBody = jsonEncode({
      "username": username,
      "password": password,
    });
    var response = await http.post(Uri.parse(_url + "login"), body: encodeBody);
    print(response.body);
    var decodeResponse = jsonDecode(response.body);
    if (decodeResponse["status"]["type"] == "success") {
      User.email = decodeResponse["data"]["user"]["email"];
      User.userName = decodeResponse["data"]["user"]["username"];
      User.childName = decodeResponse["data"]["user"]["childName"];

      _sharedPreferences!
          .setString("username", decodeResponse["data"]["user"]["username"]);
      _sharedPreferences!
          .setString("childName", decodeResponse["data"]["user"]["childName"]);
      _sharedPreferences!
          .setString("email", decodeResponse["data"]["user"]["email"]);
      _sharedPreferences!.setBool("isLogged", true);

      return 1;
    } else if (decodeResponse["status"]["message"] ==
        "Username or password incorrect")
      return -2;
    else
      return 0;
  }

  static Future<int> textClassifier(String text) async {
    _sharedPreferences = await SharedPreferences.getInstance();
    String? username =_sharedPreferences!.getString("username");

    var encodeBody = jsonEncode({
      "text": text,
      "username": username,
    });
    var response =
        await http.post(Uri.parse(_url + "textClassifier"), body: encodeBody);
    print(response.body);
    var decodeResponse = jsonDecode(response.body);
    //the word is clean
    if (decodeResponse["status"]["message"] == "Positive") {
      return 1;
    } else {
      return -1;
    }

    return 0;
  }
}
